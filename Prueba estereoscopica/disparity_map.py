import numpy as np
import cv2
from matplotlib import pyplot as plt

'''
    Cargar imagenes para pruebas
'''
imgL = cv2.imread('tsukuba_l.png', 0)
imgR = cv2.imread('tsukuba_r.png', 0)


def my_stereo_BM_create(
        imgL=None,
        imgR=None,
        numDisparities=16,
        blockSize=15):

    '''
        Generacion de imagen Stereoscopica
    :param imgL: recurso fuente Izquierdo
    :param imgR: recurso fuente Derecho
    :param numDisparities: mirar doc.
    :param blockSize: mirar dock.
    :return:
    '''

    plt.clf()
    stereo = cv2.StereoBM_create(
        numDisparities=numDisparities,
        blockSize=blockSize,
    )

    plt.figure(1)

    plt.subplot2grid((2, 2), (0, 0))
    plt.imshow(imgL, 'gray')
    plt.title("Imagen Izquierda")

    plt.subplot2grid((2,2), (0, 1))
    plt.imshow(imgR, 'gray')
    plt.title("Imagen Derecha")

    plt.subplot2grid((2, 2), (1, 0), rowspan=2)
    disparity = stereo.compute(imgL, imgR)
    iStereo = plt.imshow(disparity)
    plt.colorbar(iStereo)
    plt.draw()
    plt.pause(0.1)



blSize = np.arange(7, 30, 2)

if __name__ == "__main__":

    STATE = 0
    # STATE = 1

    if STATE == 0:
        """
            UNICA ITERACION
        """
        my_stereo_BM_create(imgL=imgL,
                            imgR=imgR,
                            numDisparities=32,
                            blockSize=31)
        print("presione tecla para continuar")
        plt.show()

    if STATE == 1:
        """
            SIMULACION CON DIFERENTES BLOCKSIZE
        """

        for i in range(len(blSize)):
            my_stereo_BM_create(imgL=imgL,
                                imgR=imgR,
                                numDisparities=16,
                                blockSize=blSize[i])
            print("BlockSize: {}".format(blSize[i]))


