from numpy import *
import cv2
from matplotlib import pyplot as plt

# NOTA: Todo lo que tenga el numero 2 pertenece a la camara derecha

# Asigno las camaras a una variable y el tamano de la captura
cap = cv2.VideoCapture(1)
cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 480)
cap2 = cv2.VideoCapture(2)
cap2.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 320)
cap2.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 240)

# Criterio de terminacion
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 12, 0.001)
criteria2 = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 12, 0.001)

# Preparando puntos, (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = zeros((9*6,3), float32)
objp[:,:2] = mgrid[0:9,0:6].T.reshape(-1,2)
objp2 = zeros((9*6,3), float32)
objp2[:,:2] = mgrid[0:9,0:6].T.reshape(-1,2)
# Arreglos para los puntos de las imagenes.
objpoints = [] # 3d puntos en el espacio real, (x25mm) medida de los cuadros del tablero
imgpoints = [] # 2d puntos en el plano de imagenes.
objpoints2 = [] 
imgpoints2 = []

enum = 1

while(enum <= 12):
    ret, frame = cap.read() # leo la captura de la camara izquierda y la guardo en frame
    ret2, frame2 = cap2.read()# leo la captura de la camara derecha y la guardo en frame
    gray = cv2.cvtColor(frame.copy(),cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(frame2.copy(),cv2.COLOR_BGR2GRAY)
    cv2.imshow('Calibracion', gray)
    cv2.imshow('Calibracion2', gray2)
    # Encuentro las esquinas del patron
    ret3, corners = cv2.findChessboardCorners(gray, (9,6),None)
    ret4, corners2 = cv2.findChessboardCorners(gray2, (9,6),None)
    
    # Si entuentra las esquinas, agregar esos puntos
    if  ret3 == True and ret4 == True and enum <= 12:
        objpoints.append(objp*25)
        objpoints2.append(objp*25)

        cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        imgpoints.append(corners)
        cv2.cornerSubPix(gray2,corners2,(11,11),(-1,-1),criteria2)
        imgpoints2.append(corners2)

        # Dibujar y mostrar las esquinas sobre la imagen
        cv2.drawChessboardCorners(frame, (9,6),corners,ret3)
        cv2.imshow('Captura de esquinas',frame)
        cv2.waitKey(1000)
        cv2.imwrite('/home/jorge/Vision_3D/calibCAMI_{0}.jpg'.format(enum),frame)
        cv2.drawChessboardCorners(frame2, (9,6),corners2,ret4)
        cv2.imshow('Captura de esquinas2',frame2)
        cv2.waitKey(1000)
        cv2.imwrite('/home/jorge/Vision_3D/calibCAMD_{0}.jpg'.format(enum),frame2)
        print enum

        # Calibramos las camaras con los puntos obtenidos
        if enum == 12:
            ret3, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
            ret4, mtx2, dist2, rvecs2, tvecs2 = cv2.calibrateCamera(objpoints2, imgpoints2, gray2.shape[::-1],None,None)

            img = cv2.imread('/home/jorge/Vision_3D/calibCAMI_'+'7.jpg')

            img2 = cv2.imread('/home/jorge/Vision_3D/calibCAMD_'+'7.jpg')
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
            h,  w = img.shape[:2]           #a (1 to see the whole picture)
            h2,  w2 = img2.shape[:2]           #a (1 to see the whole picture)
            newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
            newcameramtx2, roi2=cv2.getOptimalNewCameraMatrix(mtx2,dist2,(w2,h2),1,(w2,h2))
            if (size(roi) == 4 and size(roi2) == 4 and mean(roi) != 0 and mean(roi2) != 0):
                # Sin distorcion
                mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)
                mapx2,mapy2 = cv2.initUndistortRectifyMap(mtx2,dist2,None,newcameramtx2,(w2,h2),5)
                dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)
                dst2 = cv2.remap(img2,mapx2,mapy2,cv2.INTER_LINEAR)
                print('vamos bien')
                # Grabo la imagen y todos los parametros de las camaras
                x,y,w,h = roi
                x2,y2,w2,h2 = roi2
                dst = dst[y:y+h, x:x+w]
                dst2 = dst2[y2:y2+h2, x2:x2+w2]
                dst = cv2.cvtColor(dst,cv2.COLOR_RGB2BGR)
                dst2 = cv2.cvtColor(dst2,cv2.COLOR_RGB2BGR)
                plt.imshow(dst)
                plt.imshow(dst2)
                cv2.imwrite('Imagen_distorcionada_Camara_Izq.jpg',dst)
                cv2.imwrite('Imagen_distorcionada_Camara_Der.jpg',dst2)
                savetxt('mtxI.txt',mtx)
                savetxt('camaramtxI.txt',newcameramtx)
                savetxt('mapxI.txt',mapx)
                savetxt('mapyI.txt',mapy)
                savetxt('distI.txt',dist)
                savetxt('mtxD.txt',mtx2)
                savetxt('camaramtxD.txt',newcameramtx2)
                savetxt('mapxD.txt',mapx2)
                savetxt('mapyD.txt',mapy2)
                savetxt('distD.txt',dist2)
                print('Terminamos')
            else:
                disp('Algo No se hizo bien')
        enum += 1

    # ciclo para romper el while y cerrar las capturas
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cap2.release()
cv2.destroyAllWindows()
